import subprocess
import csv
from yaml import load
import os
from ast import literal_eval


def extract_snps_in_transcripts(df_snps, row, dict_args):

    utr_loci = literal_eval(row['Loci'])
    regions = []
    for item in utr_loci:
        regions.append(item[0:-2])
    filters = '-r '+ ','.join(regions)
    variant_features = generate_query_cmd(dict_args['vcf_file'], filters, dict_args['features'])

    for entry in variant_features:
        dict_row = {}
        for f in row.keys():
            dict_row.update({f: row[f]})
        dict_row.update(dict(zip(dict_args['features'], entry.split())))
        dict_row.update({'vcf_src': dict_args['vcf_src']})
        #print(dict_row)
        df_snps = df_snps.append(dict_row, ignore_index = True)

    return df_snps


def generate_query_cmd(vcf_file, filters, features):

    feature_cmd = "-f '"
    for feature in features:
        feature_cmd = feature_cmd + "%" + feature + ' '
    feature_cmd = feature_cmd[0:-1] + r"\n'"

    cmd = "bcftools query " + vcf_file + " " + filters + " " + feature_cmd

    stdout = subprocess.check_output(cmd, shell=True)
    variant_features = stdout.decode('utf-8').splitlines()
    return variant_features


def extract_snp_feature2CSV(vcf_file, filters, features, out_csv):
    """
    This function is used to extract variants' features from a .vcf file with filters.
    One bcftools command example:
    bcftools query /media/MyBookDuo/SNP/SNP_1XGenomes/biallelic_SNVs_and_INDELs_GRCh38/ALL.chr10.shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz
    -r 10:69401136-69401882 -i 'MAF>0.05' -f '%CHROM %END %AF\n'
    More info about bcftools query, please refer to http://samtools.github.io/bcftools/bcftools.html#query
    Columns in the output .csv file will correspond to the parameters set after -f
    :param vcf_file: the .vcf file containing all the called variants
    :param filters: parameters for bcftools query command, e.g., -r 10:69401136-69401882 -i 'MAF>0.05'
    :param features: list of features to be extracted, e.g., ['CHROM', 'END', 'AF']
    :param out_csv: name and path for the output .csv file
    :return:
    """
    out_file = open(out_csv, 'w')
    feature_writer = csv.writer(out_file, delimiter=',')
    feature_writer.writerow(features)

    variant_features = generate_query_cmd(vcf_file, filters, features)

    for entry in variant_features:
        feature_writer.writerow(entry.split())
        #print(entry.split())
    out_file.close()


def extract_sample_list(vcf_file):
    """
    This function is used to extract the list of samples' ID for calling the variants
    :param vcf_file: the .vcf file containing all the called variants
    :return: list_sample
    """
    cmd = 'bcftools query -l ' + vcf_file
          #/media/MyBookDuo/SNP/SNP_1XGenomes/biallelic_SNVs_and_INDELs_GRCh38/ALL.chr10.shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz'
    stdout= subprocess.check_output(cmd, shell=True)
    list_samples = stdout.decode('utf-8').splitlines()

    return list_samples


if __name__ == '__main__':
    #vcf_file = "/media/MyBookDuo/SNP/SNP_1XGenomes/biallelic_SNVs_and_INDELs_GRCh38/ALL.wgs.shapeit2_integrated_snvindels_v2a.GRCh38.27022019.sites.vcf.gz"
    #filters = "-r 10:69401136-69401882,X:156020000-156031000 -i 'MAF>0.05'"
    #features = ['CHROM', 'END', 'AF', 'EAS_AF', 'EUR_AF', 'AFR_AF', 'AMR_AF', 'SAS_AF']
    #csv_file = '/media/MyBookDuo/SNP/test.csv'
    yaml_file =  os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cfg_vcfExtract.yml')
    dict_args = load(open(yaml_file, 'r'))

    extract_snp_feature2CSV(dict_args['vcf_file'], dict_args['filters'], dict_args['features'], dict_args['csv_file'])