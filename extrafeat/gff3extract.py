"""
This module is used to exact desired features from a .gff3 file and save the features in a .csv file
"""

from BCBio import GFF
import pandas as pd
from yaml import load
import logging


def extract_feature_from_one_entry(feature, dict_args):

    dict_feature = []
    # if the required type of functional region doesn't share parent with another one
    if feature.type == dict_args['type_2_extract']:
        dict_feature.append(extract_feature(feature, dict_args))

    elif feature.type == 'inferred_parent':
        for sub_feature in feature.sub_features:
            dict_feature.append(extract_feature(sub_feature, dict_args))

    else:
        logging.error('--unexpected functional region type %s' %feature.type)

    return dict_feature



def extract_feature(feature, dict_args):

    # get the general features
    dict_general_feature = {key: getattr(feature, key) for key in dict_args['general_features']}
    dict_qualifier = dict.fromkeys(dict_args['features_in_qualifiers'])
    # get features from qualifiers
    if set(dict_args['features_in_qualifiers']).issubset(set(list(feature.qualifiers.keys()))):
        dict_qualifier = {key: feature.qualifiers[key][0] for key in dict_args['features_in_qualifiers']}

    elif set(dict_args['features_in_qualifiers']).intersection(set(list(feature.qualifiers.keys()))):
        logging.warning(
            '--one or more of the features required in the qualifiers do not exist in the qualifiers of %s' % feature.id)
        dict_qualifier = {key: feature.qualifiers[key][0] for key in
                          set(dict_args['features_in_qualifiers']).intersection(set(list(feature.qualifiers.keys())))}
    else:
        logging.error(
            '--none of the features exists in the qualifiers, please check')

    return {**dict_general_feature, **dict_qualifier}



def extract_feature_from_gff3(yaml_file):

    # obtain args from YAML file
    stream = open(yaml_file, 'r')
    dict_args = load(stream)


    # create the data frame with required features as columns
    df_transcript = pd.DataFrame(columns= dict_args['features_in_qualifiers'] + dict_args['general_features'])

    # set the extraction limits
    limit_info = dict(
            # gff_id = ['1'],
            gff_type = [dict_args['type_2_extract']]
            )

    # open the .gff3 file
    in_handle = open(dict_args['gff_file'])

    # start the extraction process
    for rec in GFF.parse(in_handle, limit_info=limit_info):
        for feature in rec.features:
            list_dict = extract_feature_from_one_entry(feature, dict_args)
            for extract_feature in list_dict:
                if bool(extract_feature):
                    df_transcript = df_transcript.append(extract_feature, ignore_index=True)


    # if ' (assigned to previous version ' in sub_feature.qualifiers['transcript_support_level'][0]:
    # if 'transcript_support_level' in dict_args['features_in_qualifiers']:
        # df_transcript['transcript_support_level'] = df_transcript['transcript_support_level'].apply(lambda x: x[0])

    # write the data frame to a .csv file
    # print(df_transcript)
    df_transcript.to_csv(dict_args['csv_2_write'], index=False)
    in_handle.close()


extract_feature_from_gff3('gff3extract.yml')