"""
This script is used to generate the consensus sequences for the transcripts extracted from SQLite.
After obtaining the transcripts by SQL query, the transcripts will be 'filtered' by the following conditions:
keep only one transcript for each gene, which has the best TSL and longest length.
"""
from os.path import basename

import pandas as pd
from sqlalchemy import create_engine
from ast import literal_eval
import os
import subprocess
import sys
sys.path.extend(['/home/yafei/Dropbox (UiO)/Projects/KNOCK'])
from general import getLogger
from all_classes import Sample, Transcript
import shlex
import multiprocessing as mp
from yaml import load
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from extrafeat import vcfExtract


# get super population and sub population information
script_path = os.path.dirname(os.path.realpath(__file__))
ref_fa_path = '/'.join(['/media/MyBookDuo', 'ref.fa'])
pop_file = os.path.join(script_path, 'super_sub_pop.yml')
dict_pop = load(open(pop_file, 'r'))


def update_path(path, list_subdir):
    """
    This function is used to update paths in a dict with an ordered list of sub-folders' names. It also checks the
    existence of the sub-paths, if not, the sub-paths will be created.
    :param dict_path: dict, paths for different usage output
    :param list_subdir: list, list of more detailed categorisations as sub-folders' names
    :return:
    """

    new_path = os.path.join(path, '/'.join(list_subdir))
    os.makedirs(new_path, exist_ok=True)

    return  new_path


def consensus_seq_all_pops(sample, utr, vcf_file, filters, out_file):
    """
    This function writes the consensus sequence of a sample to a file containing the consensus sequences
    from all samples using samtools and bcftools.
    :param ref_genome: str, the file of reference genome
    :param sample: Sample obj, an object of Sample class, containing ID and population information
    :param region: str, the interested chromosome region
    :param vcf_file: str, the file of SNVs called for the sample
    :param out_file: str, the file for storing the consensus sequences of all samples
    """
    logger.info('--start to generate consensus sequence')

    # bcftools consensus -f ref.fa /media/MyBookDuo/SNP/SNP_1XGenomes/ALL.chrX_GRCh38.genotypes.20170504.vcf.gz -s HG01852
    consensus_cmd = "bcftools consensus -f " + ref_fa_path + " " + vcf_file + " -s " + sample.id + filters
    # print(consensus_cmd)

    stdout = subprocess.check_output(consensus_cmd, shell=True)

    # seq = stdout.decode('utf-8')[len_str_locus + 2:]
    full_seq = stdout.decode('utf-8').splitlines()
    my_seq = [x for x in full_seq if '>' not in x]
    my_seq = ''.join(my_seq)
    my_seq = Seq(my_seq, IUPAC.unambiguous_dna)

    if utr.strand == '-':
        my_seq = my_seq.reverse_complement()

    content = sample.sup_pop + '___' + sample.sub_pop + '___' + sample.id

    rec = SeqRecord(my_seq, id=content, description='')
    # write all consensus sequences to one .fa file
    SeqIO.write(rec, out_file, 'fasta')
    logger.info('--consensus done')



def generate_consensus(sample_file, utr, out_path, vcf_path, ref_genome, filters, sub_folder):
    """
    This function starts the process of downloading reads, SNV calling and consensus sequence generation for each 3'utr
    locus.
    :param sample_file: str, the file for storing sample files.
    :param utr_loci: list, a list of locus for one 3'utr
    :param paths: dict, containing paths for all types of output
    """
    logger.info('--Start all processes')


    ref_fa_cmd = 'samtools faidx ' + ref_genome + ' ' + utr.region_UCSC + ' |tr -d "chr" > ' + shlex.quote(ref_fa_path)
    subprocess.run(ref_fa_cmd, shell=True)

    # open the file for writing consensus sequences in
    filename = utr.gene+'_'+utr.trans_id
    if sub_folder:
        filename = filename+'_'+sub_folder
    filename = filename +'_'+'consensus.fa'

    consensus_file = open(os.path.join(out_path, filename),'w')

    vcf_file = os.path.join(vcf_path,
                            'ALL.chr' + utr.chr + '.shapeit2_integrated_snvindels_v2a_27022019.GRCh38.phased.vcf.gz')

    df_sample = pd.read_csv(sample_file, sep=',')
    for _, row in df_sample.iterrows():

        sample = Sample(id=row['Sample_ID'], sub_pop=row['Sub_Pop'], sup_pop=row['Super_Pop'])

        logger.info('--process file %s' %sample.id)
        # step3: consensus sequence
        consensus_seq_all_pops(sample, utr, vcf_file, filters, consensus_file)

    # close output file for each locus
    consensus_file.close()
    logger.info('--all samples done')


def proc_loci_list(trans):
    logger.info('--start to obtain transcript loci')
    utr_loci = literal_eval(trans['Loci'])

    strand_utr = utr_loci[-1][-1]

    chr_utr = utr_loci[0][0: utr_loci[0].find(':')]

    regions = [locus[0:-2] for locus in utr_loci]
    chr_loci = ['chr' + locus[0:-2] for locus in utr_loci]

    utr = Transcript(region_UCSC=' '.join(chr_loci), region_EMBL=','.join(regions), strand= strand_utr, chr=chr_utr, gene=trans['Genename'], trans_id=trans['transcript_id'])
    return utr


def process_consensus(consensus_path, vcf_path, row, sample_file, ref_genome, filters, sub_folder):

    logger.info('--start to process transcript %s' % row['transcript_id'])
    utr = proc_loci_list(row)

    out_consensus_path = update_path(consensus_path, [row['Genename'], row['transcript_id'], sub_folder])
    # print(out_consensus_path)

    generate_consensus(sample_file, utr, out_consensus_path, vcf_path, ref_genome, filters, sub_folder)



def load_input(yml_file):


    if not os.path.isfile(yml_file):
        yml_file =  os.path.join(os.path.dirname(os.path.realpath(__file__)), yml_file)
    dict_args = load(open(yml_file, 'r'))

    return dict_args


def proc_df_trans_snp_cols(dict_args):

    super_pop_AF = ['EAS_AF', 'EUR_AF', 'AFR_AF', 'AMR_AF', 'SAS_AF']
    feature_snps = []
    if len(set(super_pop_AF) & set(dict_args['features'])) > 2 and dict_args['merge_super_pop_AF']:
        for item in dict_args['features']:
            if not item in set(super_pop_AF) & set(dict_args['features']):
                feature_snps.append(item)
        feature_snps += ['super_pop', 'pop_AF']
    else:
        feature_snps = dict_args['features']
    return feature_snps


def run_case():

    dict_args = load_input('cfg_PT_test.yml')
    #print(" -i " + dict_args['consensus_filters'])

    # load the table of transcripts

    df_trans = pd.read_csv(dict_args['transcript_file'], sep=',')
    #print(list(df_trans.columns))
    super_pop_AF = ['EAS_AF', 'EUR_AF', 'AFR_AF', 'AMR_AF', 'SAS_AF']

    # extract transcripts related snps if required
    if dict_args['extract_snps']:

        #feature_snps = proc_df_trans_snp_cols(dict_args)
        feature_snps = dict_args['features']
        df_snps = pd.DataFrame(columns=list(df_trans.columns)+feature_snps+['vcf_src'])
        #print(list(df_snps.columns))

        for _, row in df_trans.iterrows():
            #print([key for key in row.keys()], type([key for key in row.keys()]))
            df_snps = vcfExtract.extract_snps_in_transcripts(df_snps, row, dict_args)
        df_snps[['AF']+super_pop_AF] = df_snps[['AF']+super_pop_AF].apply(pd.to_numeric)
        df_snps_flat = pd.melt(df_snps,id_vars=list(set(list(df_snps.columns))-set(super_pop_AF)), value_vars=super_pop_AF)


        df_snps.to_csv(dict_args['out_snp_csv'], sep=',', index=False)
        df_snps_flat.to_csv(dict_args['out_snp_csv_flat'], sep=',', index=False)


    # generate consensus sequences if required
    if dict_args['generate_consensus']:

        if not dict_args['consensus_filters']:
            dict_args['consensus_filters'] = ['']
        if not dict_args['sub_folder_name']:
            dict_args['sub_folder_name'] = ['']

        if len(dict_args['consensus_filters']) != len(dict_args['sub_folder_name']):
            logger.error('--the number of vcf filters is not the same as the number of sub-folders, please check your .yml file')


        for _, row in df_trans.iterrows():
            for idx in range(len(dict_args['consensus_filters'])):
                process_consensus(dict_args['consensus_path'], dict_args['vcf_path'], row, dict_args['sample_file'], dict_args['ref_genome'], dict_args['consensus_filters'][idx], dict_args['sub_folder_name'][idx])

    if dict_args['conduct_MSA']:
        for (dirpath, dirnames, filenames) in os.walk(dict_args['consensus_path']):
            for filename in filenames:
                if dict_args['keyword_MSA'] in filename and 'aligned_' not in filename:
                    consensus_file = os.sep.join([dirpath, filename])
                    out_file = os.sep.join([dirpath, 'aligned_' + filename])

                    # cmd = 'time muscle -in ' + consensus_file + ' -out ' + out_file + '2>&1 > /Users/yafeix/Downloads/log.txt'
                    cmd = 'muscle -in ' + consensus_file + ' -out ' + out_file
                    subprocess.check_output(cmd, shell=True)
                    #print(sys.stdout.decode('utf-8'))

def main():



    run_case()




if __name__ == '__main__':

    logger = getLogger.getlogger(basename(__file__))

    main()
