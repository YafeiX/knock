import pandas as pd
from plotnine import *

df=pd.read_csv('/Users/yafeix/Dropbox (UiO)/Projects/pathway/data/R-HSA-70326/3utr_500_2000_TSL12_R_HSA_70326.csv')
gplot1 = ggplot(df, aes('loci_length')) + geom_histogram(bins=40, color='orange',fill='orange') + \
             labs(x='3UTR loci length', y='count of 3UTRs/transcripts', title="Distribution of the 3UTR loci length of the selected transcripts related with R_HSA_70326")
print(gplot1)

df=pd.read_csv('/Users/yafeix/Dropbox (UiO)/Projects/pathway/data/R-HSA-70326/number_trans_per_gene.csv')
gplot2 = ggplot(df, aes('number_of_transcript')) + geom_histogram(bins=3, color='orange',fill='orange') + \
            geom_text(aes(label='stat(count)'), stat='count', va='bottom') +\
             labs(x='number of transcripts per gene', y='count of genes', title="Distribution of number of transcripts per gene related with R_HSA_70326")
print(gplot2)


