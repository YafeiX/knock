from yaml import load
import os
import csv
import logging
import pandas as pd



def extract_all_samples2CSV(sample_file, dict_pop_yaml, out_file):
    """
    This function is only used to obtain all samples' features from 1KGenomes project's index file. It contains sample
    features in the format of ftp:/ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/data/GBR/HG04302/alignment/HG04302.alt_bwamem_GRCh38DH.20150718.GBR.low_coverage.cram
    :param sample_file: the path of the .index file
    :param dict_pop: dict of sub-population and super-population given in .yaml format
    :param out_file: the output .csv file with columns of 'Sample_ID', 'Sub_Pop', 'Super_Pop'
    """
    # load dict_pop_yaml
    if os.path.isfile(dict_pop_yaml):
        dict_pop = load(open(dict_pop_yaml, 'r'))
    else: #use the default path
        script_path = os.path.dirname(os.path.realpath(__file__))
        pop_file = os.path.join(script_path, dict_pop_yaml)
        dict_pop = load(open(pop_file, 'r'))

    csv_file = open(out_file, 'w')
    sample_writer = csv.writer(csv_file, delimiter=',')
    sample_writer.writerow(['Sample_ID', 'Sub_Pop', 'Super_Pop'])

    if os.path.isfile(sample_file):
        with open(sample_file, 'r') as indices:
            for index in indices.readlines():
                # process the sample info from the name of the index file and prepare the output file
                index = index.rstrip()

                ind = index.rfind('/')
                file_name = index[ind + 1:]

                # get the sample ID
                sample_ID = file_name[0:7]

                country = file_name[37:40]
                if country in dict_pop.keys():
                    sup_pop = dict_pop[country]
                else:
                    logging.error('There is not %s sub-population in the population dict, please check',country)

                sample_writer.writerow([sample_ID, country, sup_pop])
    csv_file.close()


def extract_samples_from_CSV(sample_csv, list_samples, out_csv):
    """
    This function is used to obtain the features of indicated samples (indicated in list_samples) from the .csv file
    of the samples
    :param sample_csv:  the input .csv file of samples, columns are 'Sample_ID', 'Sub_Pop', 'Super_Pop'
    :param list_samples: list of IDs of samples to be extracted
    :param out_csv: the output .csv file
    :return:
    """

    df_sample = pd.read_csv(sample_csv, sep=',')
    inter_set = set(df_sample['Sample_ID']) & set(list_samples)
    if len(inter_set) < len(set(list_samples)):
        logging.warning('there are samples in the sample list not included in the sample .csv file')
    df_new_sample = df_sample.loc[df_sample['Sample_ID'].isin(inter_set)]
    df_new_sample.to_csv(out_csv, index=False)



if __name__ == '__main__':
    sample_file = '/Users/yafeix/Dropbox (UiO)/Projects/1KGenome/data/ori/1000genomes.low_coverage.cram.index'
    dict_pop_yaml = 'super_sub_pop.yml'
    out_file = '/Users/yafeix/Dropbox (UiO)/Projects/1KGenome/data/new/1000genomes.low_coverage.samples.csv'
    extract_all_samples2CSV(sample_file, dict_pop_yaml, out_file)