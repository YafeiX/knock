

pops = {'LWK':'AFR', 'GWD':'AFR', 'ESN':'AFR', 'FIN':'EUR', 'GBR':'EUR', 'IBS':'EUR', 'BEB':'SAS', 'PJL':'SAS', 'STU':'SAS', 'PEL':'AMR', 'CLM':'AMR', 'PUR':'AMR', 'KHV':'EAS', 'JPT':'EAS', 'CHS':'EAS'}

f_proc = '/Users/yafeix/Downloads/ENSG00000221983_chr19\:18575151-18577550_consensus.fa'

f_bash = '/Users/yafeix/Downloads/changename.sh'

with open(f_bash, 'w') as fwrite:
    for pop in pops.keys():
        fwrite.write(" sed -i -e 's/" + pop + "/" + pops[pop] + "_" + pop + "/g' " + f_proc + '\n')